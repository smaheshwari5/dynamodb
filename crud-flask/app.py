from flask import Flask, request
import dynamodb_handler as dynamodb
app = Flask(__name__)


                           ##################################     Table  Create      #########################################
@app.route('/')
def root_route():
    # dynamodb.CreatATableBook()
    return '----------     Here we are creating our Dynamodb Table    ----------- '
                           

                             ##################################     Book Post      #########################################
@app.route('/book/post', methods=['POST'])
def addABook():

    data = request.get_json()
    response = dynamodb.addItemToBook(data['id'], data['title'], data['author'],data['like'], data['city'], data['mobile'])    
    if (response['ResponseMetadata']['HTTPStatusCode'] == 200):
        return {
            'msg': 'Added successfully',
        }

    return {  
        'msg': 'Some error occcured',
        'response': response
    }

 
                               ##################################     Book Get      #########################################
@app.route('/book/get/<int:id>', methods=['GET'])
def getBook(id):
    response = dynamodb.GetItemFromBook(id)
    
    if (response['ResponseMetadata']['HTTPStatusCode'] == 200):
        
        if ('Item' in response):
            return { 'Item': response['Item'] }

        return { 'msg' : 'Item not found!' }

    return {
        'msg': 'Some error occured',
        'response': response
    }


                       ##################################     Book Update      #########################################
@app.route('/book/update/<int:id>', methods=['PUT'])
def UpdateABook(id):

    data = request.get_json()

    response = dynamodb.UpdateItemInBook(id, data)

    if (response['ResponseMetadata']['HTTPStatusCode'] == 200):
        return {
            'msg'                : 'Updated successfully',
            'ModifiedAttributes' : response['Attributes'],
            'response'           : response['ResponseMetadata']
        }

    return {
        'msg'      : 'Some error occured',
        'response' : response
    }   


                        ##################################     Book Delete      #########################################
@app.route('/book/delete/<int:id>', methods=['DELETE'])
def DeleteABook(id):

    response = dynamodb.DeleteAnItemFromBook(id)

    if (response['ResponseMetadata']['HTTPStatusCode'] == 200):
        return {
            'msg': 'Deleted successfully',
        }

    return {  
        'msg': 'Some error occcured',
        'response': response
    } 

 






if __name__ == '__main__':
    app.run(host='localhost', port=5000, debug=True)