import boto3
# from decouple import config
AWS_ACCESS_KEY_ID = 'AKIAZIUY3EHPNIBCNKSC'
AWS_SECRET_ACCESS_KEY = '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'
REGION_NAME = 'us-east-1'


client = boto3.client(
    'dynamodb',
    aws_access_key_id     = AWS_ACCESS_KEY_ID,
    aws_secret_access_key = AWS_SECRET_ACCESS_KEY,
    region_name           = REGION_NAME,
)
resource = boto3.resource(
    'dynamodb',
    aws_access_key_id     = AWS_ACCESS_KEY_ID,
    aws_secret_access_key = AWS_SECRET_ACCESS_KEY,
    region_name           = REGION_NAME,
)

                                 ##################################     Table  Create      #########################################
def CreatATableBook():
        
    client.create_table(
        AttributeDefinitions = [ 
            {
                'AttributeName': 'id', 
                'AttributeType': 'N'  
            }
        ],
        TableName = 'Book', 
        KeySchema = [       
            {
                'AttributeName': 'id',
                'KeyType'      : 'HASH' 
                
            }
        ],
        BillingMode = 'PAY_PER_REQUEST',
        Tags = [ 
            {
                'Key' : 'test-resource',
                'Value': 'dynamodb-test'           
            }
        ]
    )

                                ##################################     Book Post      #########################################

BookTable = resource.Table('Book')


def addItemToBook(id, title, author,like,city,mobile):
    response = BookTable.put_item(
        Item = {
            'id'     : id,
            'title'  : title,
            'author' : author,
            'like'  : like,
            'city'   : city,
            'mobile' : mobile
        }
    )    
    return response


                             ##################################     Book Get      #########################################
def GetItemFromBook(id):
    response = BookTable.get_item(
        Key = {
            'id'     : id
        },
        AttributesToGet=[
            'title', 'author','like','city', 'mobile'
        ]
    )    
    return response


                             ##################################     Book Update     #########################################
def UpdateItemInBook(id, data:dict):    
    response = BookTable.update_item(
        Key = {
            'id': id
        },
        AttributeUpdates={
            'title': {
                'Value'  : data['title'],
                'Action' : 'PUT' 
            },
            'author': {
                'Value'  : data['author'],
                'Action' : 'PUT'
            },
            'like': {
                'Value'  : data['like'],
                'Action' : 'PUT' 
            },
            'city': {
                'Value'  : data['city'],
                'Action' : 'PUT' 
            },
            'mobile': {
                'Value'  : data['mobile'],
                'Action' : 'PUT' 
            },
        },
        ReturnValues = "UPDATED_NEW" 
    )    
    return response




                            
                             ##################################     Book Delete      #########################################
def DeleteAnItemFromBook(id):    
    response = BookTable.delete_item(
        Key = {
            'id': id
        }
    )    
    return response