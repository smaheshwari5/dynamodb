import boto3
from boto3.dynamodb.conditions import Key
dynamodb = boto3.resource('dynamodb')


################################################           BATCH     WRITE          #############################################
table = dynamodb.Table('Employees')
with table.batch_writer() as batch:
    batch.put_item(
        Item={
            'emp_id': 1,
            'first_name': 'ram',
            'last_name': 'singh',
            'email': 'rsingh@test.com'

        }
    )
    batch.put_item(
        Item={
            'emp_id': 2,
            'first_name': 'rony',
            'last_name': 'jat',
            'email': 'rjat@test.com'

        }
    )
    batch.put_item(
        Item={
            'emp_id': 3,
            'first_name': 'neha',
            'last_name': 'sharma',
            'email': 'nsharma@test.com'

        }
    )
    batch.put_item(
        Item={
            'emp_id': 4,
            'first_name': 'akshara',
            'last_name': 'birla',
            'email': 'abirla@test.com'

        }
    )


###################################################           QUERY         ###################################################
response = table.query(
    IndexName='email',
    KeyConditionExpression = Key('email').eq('abirla@test.com')
    
)
print("----------   Query Details  ------------ \n")
items = response["Items"]
for item in items:
    print(item , "\n")
