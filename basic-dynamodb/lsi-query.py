import boto3
from boto3.dynamodb.conditions import Key
dynamodb = boto3.resource('dynamodb')


################################################           BATCH     WRITE          #############################################
table = dynamodb.Table('Posts')
with table.batch_writer() as batch:
    batch.put_item(
        Item={
            'title': "I love Dancing",
            'user_name': 'krati',
            'subject': 'dancing '

        }
    )
    batch.put_item(
        Item={
            'title': "I like to Read books ",
            'user_name': 'anushka',
            'subject': 'reading'

        }
    )
    batch.put_item(
        Item={
            'title': "My favorite recipes is Pav Bhaji",
            'user_name': 'radhika',
            'subject': 'cooking'

        }
    )
    batch.put_item(
        Item={
            'title': "I like reading ",
            'user_name': 'anushka',
            'subject': 'reading'

        }
    )
    batch.put_item(
        Item={
            'title': "Always study mood",
            'user_name': 'muskan',
            'subject': 'study'

        }
    )
    batch.put_item(
        Item={
            'title': "I like dancing ",
            'user_name': 'purvi',
            'subject': 'dancing'

        }
    )
    batch.put_item(
        Item={
            'title': "I like reading ",
            'user_name': 'neha',
            'subject': 'reading',
            'age':20

        }
    )



###################################################           QUERY         ###################################################
response = table.query(
    IndexName='user_name_subject',
    KeyConditionExpression = Key('user_name').eq('anushka') & Key('subject').eq('reading')
    
)
print("----------   Query Details  ------------ \n")
items = response["Items"]
for item in items:
    print(item , "\n")









