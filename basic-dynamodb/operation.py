from xml.dom.minidom import Attr
import boto3
from boto3.dynamodb.conditions import Key, Attr 

dynamodb = boto3.resource('dynamodb')


##################################################           date time of table create           #############################################

table = dynamodb.Table('Detail')
print("---------------  Date Time of Table Create  ---------------\n")
print(table.creation_date_time , "\n")


##################################################           PUT   DATA          #############################################
table = dynamodb.Table('Detail')
table.put_item(
    Item={
        'first_name':'sanjana',
        'last_name':'maheshwari',
        'age': 22,
        'mobile':9873210456,
        'city':'Amravati'
    }
)

table.put_item(
    Item={
        'first_name':'Bhumika',
        'last_name':'Sadani',
        'age': 22,
        'mobile':9638527410,
        'city':'Arvi'
    }
)

table.put_item(
    Item={
        'first_name':'Khushi',
        'last_name':'Jalan',
        'age': 22,
        'mobile':9856320147,
        'city':'Chandur'

    }
)


##################################################           GET   DATA          #############################################
# table = dynamodb.Table('Detail')
response = table.get_item(
    Key={
        'first_name':'sanjana',
        'last_name':'maheshwari'
    }
)
print("--------    Get Data    ------ \n")
print('Response :  \n', response)
item = response['Item']
print("----------   Items  ------------\n")
print(item, "\n")



##################################################           UPDATE   DATA          #############################################
# table = dynamodb.Table('Detail')

table.update_item(
    Key={
        'first_name':'sanjana',
        'last_name': 'maheshwari'
    },
    UpdateExpression='SET age = :val1',
    ExpressionAttributeValues ={
        ':val1': 21
    }
)



##################################################           DELETE   DATA          #############################################
# table = dynamodb.Table('Detail')
table.delete_item(
    Key={
        'first_name':'unknown4',
        'last_name':'unknown'
    }
)


###################################################           QUERY         ###################################################
response = table.query(
    KeyConditionExpression = Key('first_name').eq('Bhumika')
    
)
print("----------   Query Details  ------------ \n")
items = response["Items"]
for item in items:
    print(item , "\n")

response = table.query(
    KeyConditionExpression = Key('first_name').eq('Employee'),
    FilterExpression=Attr('age').lt(24)
)
print("----------   Query Details  ------------ \n")
items = response["Items"]
for item in items:
    print(item , "\n")


# response = table.query(
#     KeyConditionExpression = Key('first_name').eq('Employee'),
#     ScanIndexForward=False
# )
# print("----------   Query Details  ------------ \n")
# items = response["Items"]
# for item in items:
#     print(item , "\n")


# response = table.query(
#     KeyConditionExpression = Key('first_name').eq('Employee'),
#     FilterExpression=Attr('age').lt(25) & Attr('city').eq('Pune')
# )
# print("----------   Query Details  ------------ \n")
# items = response["Items"]
# for item in items:
#     print(item , "\n")








#################################################           BATCH     WRITE          #############################################
with table.batch_writer() as batch:
    batch.put_item(
        Item={
            'first_name':'Bhumika',
            'last_name':'Rangari',
            'age': 22,
            'mobile':9879879870, 
            'city':'Chandur'

        }
    )
    batch.put_item(
        Item={
            'first_name':'Mitali',
            'last_name':'Agrawal',
            'age': 21,
            'mobile':8974563210,
            'city':'Paratwada'

        }
    )
    batch.put_item(
        Item={
            'first_name':'Pranjali',
            'last_name':'Patil',
            'age': 22,
            'mobile':9856320147,
            'city':'Amravati'

        }
    )
    batch.put_item(
        Item={
            'first_name':'Shruti',
            'last_name':'Agrawal',
            'age': 23,
            'mobile':8574963210,
            'city':'Aachalpur'

        }
    )



##################################################           BATCH  WRITER with range           #############################################

with table.batch_writer() as batch:
    for i in range(5):
        batch.put_item(
            Item={
                'first_name':'user' + str(i),
                'last_name':'data',
                'age': 24,
                'mobile': 1478523690,
                'city':'Pune'
                
            }
        )





#################################################           TABLE   DELETE         #############################################
# table = dynamodb.Table('Detail')
# table.delete()
# print("----- Table Successfully Deleted --------")






