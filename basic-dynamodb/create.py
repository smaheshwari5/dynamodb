import boto3

dynamodb = boto3.resource('dynamodb')


##################################################           CREATE TABLE         #############################################

table = dynamodb.create_table(
        TableName='Detail',
        KeySchema=[
            {
                'AttributeName': 'first_name',
                'KeyType': 'HASH'  # Partition key
            },
            {
                'AttributeName': 'last_name',
                'KeyType': 'RANGE'  # Sort key
            }
        ],
        AttributeDefinitions=[
            {
                'AttributeName': 'first_name',
                'AttributeType': 'S'
            },
            {
                'AttributeName': 'last_name',
                'AttributeType': 'S'
            },

        ],
        ProvisionedThroughput={
            'ReadCapacityUnits': 5,
            'WriteCapacityUnits': 5
        }
)

