import boto3

dynamodb = boto3.resource('dynamodb')


##################################################           CREATE TABLE         #############################################

table = dynamodb.create_table(
        TableName='Product',
        KeySchema=[
            {
                'AttributeName': 'productid',
                'KeyType': 'HASH'  # Partition key
            },
            {
                'AttributeName': 'productname',
                'KeyType': 'RANGE'  # Sort key
            }
        ],
        AttributeDefinitions=[
            {
                'AttributeName': 'productid',
                'AttributeType': 'N'
            },
            {
                'AttributeName': 'productname',
                'AttributeType': 'S'
            },

        ],
        ProvisionedThroughput={
            'ReadCapacityUnits': 5,
            'WriteCapacityUnits': 5
        }
)

