import boto3
from boto3.dynamodb.conditions import Key, Attr 

dynamodb = boto3.resource('dynamodb')


##################################################           date time of table create           #############################################

table = dynamodb.Table('GameScore')
print("---------------  Date Time of Table Create  ---------------\n")
print(table.creation_date_time , "\n")



#################################################           BATCH     WRITE          #############################################
with table.batch_writer() as batch:
    batch.put_item(
        Item={
            'UserId':1,
            'GameTitle':'Chess',
            'TopScore':10,
            'Wins': 8,
            'Losses': 2,

        }
    )
    batch.put_item(
        Item={
            'UserId':2,
            'GameTitle':'Ludo',
            'TopScore':9,
            'Wins': 7,
            'Losses': 3,

        }
    )
    batch.put_item(
        Item={
            'UserId':3,
            'GameTitle':'S & L',
            'TopScore':8,
            'Wins': 5,
            'Losses': 5,

        }
    )
    batch.put_item(
        Item={
            'UserId':4,
            'GameTitle':'Carom',
            'TopScore':10,
            'Wins': 7,
            'Losses': 3,

        }
    )
    batch.put_item(
        Item={
            'UserId':5,
            'GameTitle':'Ludo',
            'TopScore':10,
            'Wins': 10,
            'Losses': 0 

        }
    )
    batch.put_item(
        Item={
            'UserId':6,
            'GameTitle':'Ludo',
            'TopScore':10,
            'Wins': 10,
            'Losses': 0,

        }
    )
    batch.put_item(
        Item={
            'UserId':7,
            'GameTitle':'Chess',
            'TopScore':10,
            'Wins': 8,
            'Losses': 2,

        }
    )
    batch.put_item(
        Item={
            'UserId':8,
            'GameTitle':'Race',
            'TopScore':10,
            'Wins': 6,
            'Losses': 4,

        }
    )
    batch.put_item(
        Item={
            'UserId':9,
            'GameTitle':'Bussiness',
            'TopScore':10,
            'Wins': 5,
            'Losses': 5,

        }
    )