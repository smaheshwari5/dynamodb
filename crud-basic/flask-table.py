import boto3
from botocore.exceptions import ClientError
from pprint import pprint
from decimal import Decimal
import time


client = boto3.client('dynamodb')

############################################                Create DynamoDB table                  ######################################
def create_movie_table():
    table = client.create_table(
        TableName='Data',
        KeySchema=[
            {
                'AttributeName': 'id',
                'KeyType': 'HASH'  # Partition key
            },
            {
                'AttributeName': 'first_name',
                'KeyType': 'RANGE'  # Sort key
            }
        ],
        AttributeDefinitions=[
            {
                'AttributeName': 'id',
                'AttributeType': 'N'
            },
            {
                'AttributeName': 'first_name',
                'AttributeType': 'S'
            },

        ],
        ProvisionedThroughput={
            'ReadCapacityUnits': 10,
            'WriteCapacityUnits': 10
        }
    )
    return table



if __name__ == '__main__':
    
    movie_table = create_movie_table()
    print(" --------------          Create DynamoDB Table Successfully            ............")
    print("Table status:{}".format(movie_table))
