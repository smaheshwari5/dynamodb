import boto3
from botocore.exceptions import ClientError
from pprint import pprint
from decimal import Decimal
import time


client = boto3.client('dynamodb')

#Create DynamoDB table
def create_movie_table():
    table = client.create_table(
        TableName='Movies',
        KeySchema=[
            {
                'AttributeName': 'year',
                'KeyType': 'HASH'  # Partition key
            },
            {
                'AttributeName': 'title',
                'KeyType': 'RANGE'  # Sort key
            }
        ],
        AttributeDefinitions=[
            {
                'AttributeName': 'year',
                'AttributeType': 'N'
            },
            {
                'AttributeName': 'title',
                'AttributeType': 'S'
            },

        ],
        ProvisionedThroughput={
            'ReadCapacityUnits': 10,
            'WriteCapacityUnits': 10
        }
    )
    return table



if __name__ == '__main__':
    
    # Create DynamoDB
    movie_table = create_movie_table()
    print("Create DynamoDB succeeded............")
    print("Table status:{}".format(movie_table))
