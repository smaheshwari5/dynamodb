# AWS Dynamo DB Client

**aws.dynamodb** is a package for the [Amazon DynamoDB](https://aws.amazon.com/dynamodb/) API

To use the package, you will need an AWS account and to enter your credentials into R. Your keypair can be generated on the [IAM Management Console](https://aws.amazon.com/) under the heading *Access Keys*. Note that you only have access to your secret key once. After it is generated, you need to save it in a secure location. New keypairs can be generated at any time if yours has been lost, stolen, or forgotten. The profiles tools for working with IAM, including creating roles, users, groups, and credentials programmatically; it is not needed to *use* IAM credentials.

A detailed description of how credentials can be specified is provided. The easiest way is to simply set environment variables on the command line prior to starting R or via an `Renviron.site` or `.Renviron` file, which are used to set environment variables in R during startup (see `? Startup`). They can be also set within R:

```R
Sys.setenv("AWS_ACCESS_KEY_ID" = "AKIAZIUY3EHPNIBCNKSC",
           "AWS_SECRET_ACCESS_KEY" ="LhW5vDrU3mqW4FfpQMj1NLGUgCN9fz4Ldd6ly0f",
           "AWS_DEFAULT_REGION" = "us-east-1",
           "AWS_SESSION_TOKEN" = "json")



# Various Examples and AWS SDK code examples for Amazon DynamoDB

An Amazon Web Services and DynamoDB community lead repository containing code and examples for developing with and using [Amazon DynamoDB](https://docs.aws.amazon.com/amazondynamodb/latest/developerguide/Programming.html).

We have [IAM policies for DynamoDB](https://github.com/aws-samples/aws-dynamodb-examples/tree/master/DynamoDBIAMPolicies), [a script to load an existing table](https://github.com/aws-samples/aws-dynamodb-examples/tree/master/nosqlworkbenchscript) into NoSQL Workbench, and [CloudFormation examples](https://github.com/aws-samples/aws-dynamodb-examples/tree/master/cloudformation). As well as AWS SDK [code examples for DynamoDB in various languages](https://github.com/aws-samples/aws-dynamodb-examples/tree/master/DynamoDB-SDK-Examples), you can find each language examples here:

* [Python](./DynamoDB-SDK-Examples/python)
* [Node.js](./DynamoDB-SDK-Examples/node.js)
* [Java](./DynamoDB-SDK-Examples/java)
* [.Net](./DynamoDB-SDK-Examples/dotnet)
* [Golang](./DynamoDB-SDK-Examples/golang)
* [Rust](./DynamoDB-SDK-Examples/rust)
* [Ruby](./DynamoDB-SDK-Examples/ruby)

PS. If you are up for contributing, we welcome community pull requests.


All code in this repository is provided as is, where is. There are no guarantees, either expressed or implied. It is up to you to check what they do and utilize the code responsibly.