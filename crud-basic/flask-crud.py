from re import I
import boto3
from botocore.exceptions import ClientError
from pprint import pprint
from decimal import Decimal
import time


client = boto3.client('dynamodb')

def put_data(id, first_name, last_name, age):
    response = client.put_item(
       TableName='Data',
       Item={
            'id': {
                'N': "{}".format(id),
            },
            'first_name': {
                'S': "{}".format(first_name),
            },
            'last_name': {
                "S": "{}".format(last_name),
            },
            'age': {
                "N": "{}".format(age),
            }
            
        }
    )
    return response



def get_data(id, first_name):
    try:
        response = client.get_item(       
                TableName='Data',
                Key={
                        'id': {
                                'N': "{}".format(id),
                        },
                        'first_name': {
                                'S': "{}".format(first_name),
                        }
                    }
                )
    except ClientError as e:
        print(e.response['Error']['Message'])
    else:
        return response['Item']


def update_data(id, first_name, last_name, age, mobile, city):
    response = client.update_item(
        TableName='Data',
        Key={
            'id': {
                    'N': "{}".format(id),
            },
            'first_name': {
                    'S': "{}".format(first_name),
            }
        },
        ExpressionAttributeNames={
            '#A':'age',
            '#M': 'mobile',
            '#C': 'city'
        },
        ExpressionAttributeValues={
            ':a':{
                'N': "{}".format(age)
            },
            ':m': {
                'N': "{}".format(mobile),
            },
            ':c': {
                'S': "{}".format(city),
            }
        },
        UpdateExpression='SET #A = :a, #M = :m, #C = :c',
        ReturnValues="UPDATED_NEW"
    )
    return response


def delete_data(id, first_name, age):
    try:
        response = client.delete_item(
            TableName='Data',
            Key={
                'id': {
                    'N': "{}".format(id),
                },
                'first_name': {
                    'S': "{}".format(first_name),
                }
            },
            ConditionExpression="age <= :a",
            ExpressionAttributeValues={
                ':a': {
                    'N': "{}".format(age),
                }
            }
        )            
    except ClientError as er:
        if er.response['Error']['Code'] == "ConditionalCheckFailedException":
            print(er.response['Error']['Message'])
        else:
            raise
    else:
        return response






if __name__ == '__main__':
    

    # data_resp = put_data(7,"Shruti", "Agrawal", 23)
    # print("---------------    Insert in to DynamoDB   ----------------------")
    # pprint(data_resp, sort_dicts=False)


    # movie = get_data(1, "Sanjana")
    # if movie:
    #    print("Get an item from DynamoDB succeeded............")
    #    pprint(movie, sort_dicts=False)


    update_response = update_data(2,"Bhumika", "Rangari",23, 9632014587,"Chandur")
    print("----------------          Update and item in  DynamoDB      ----------------")
    pprint(update_response, sort_dicts=False)



    # delete_response = delete_data(4,"Khushi",23)
    # if delete_response:
    #     print("---------------        Delete an Item in DynamoDB   ----------------------")
    #     pprint(delete_response, sort_dicts=False)


