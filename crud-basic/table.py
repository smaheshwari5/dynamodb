
import boto3
dynamodb = boto3.resource('dynamodb')


def create_devices_table(dynamodb=None):
    dynamodb = boto3.resource(
        'dynamodb')
    
    table = dynamodb.create_table(
        TableName='Devices',
        KeySchema=[
            {
                'AttributeName': 'device_id',
                'KeyType': 'HASH'  # Partition key
            },
            {
                'AttributeName': 'datacount',
                'KeyType': 'RANGE'  # Sort key
            }
        ],
        AttributeDefinitions=[
            {
                'AttributeName': 'device_id',
                'AttributeType': 'S'
            },
            {
                'AttributeName': 'datacount',
                'AttributeType': 'N'
            },
        ],
        ProvisionedThroughput={
            'ReadCapacityUnits': 10,
            'WriteCapacityUnits': 10  
        }
    )
    return table

if __name__ == '__main__':
    device_table = create_devices_table()
  
    print("Status:", device_table.table_status)

